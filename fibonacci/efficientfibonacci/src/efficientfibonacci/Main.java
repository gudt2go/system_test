/*
 * This is a very simple program that generates the nth fibonacci number
 * in a very efficient way.
 *
 * We define Fibonacci numbers by the following recurrence
 * F(0) = 0,
 * F(1) = 1,
 * F(i) = F(i-1)+F(i-2) for i>=2
 *
 * Thus eacch fibonacci number is the sum of the two previous ones yielding the sequence
 * 0,1,1,2,3,5,8,13,21,34,...
 */

package efficientfibonacci;

/**
 *
 * @author Masood Fallahpoor
 */

// This class in used to read input from user
import java.util.Scanner;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //create an Scanner object in order to read input from user
        Scanner input = new Scanner( System.in );

        //show a prompt to user
        System.out.println("Please enter a nonnegative nmuber N to generate the Nth fibonacci number:");

        //read an integer number
        int n = input.nextInt();

        //check the input number to be nonnegative
        if ( n<0 )
            //the input number is not nonnegative,nothing to do
            return;
        else{
            // here is the magic,the Nth fibonacci number generation
            double goldenRatio = ( (1 + Math.sqrt(5)) / 2 );
            int nthFibonacci = (int) Math.floor( (Math.pow(goldenRatio,n) / Math.sqrt(5)) + 0.5 );
            System.out.println(nthFibonacci);
        }


    } // end method main

} //end class Main
