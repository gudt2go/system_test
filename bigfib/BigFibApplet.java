import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;
import javax.swing.*;


public class BigFibApplet extends Applet implements ActionListener
{   //blows up as it nears 5000
	Button button1=new Button("List");
	Label label1=new Label("Enter number of numbers in the sequence 1-4000");
	TextField text1=new TextField();
	TextArea text2=new TextArea();
	int n;
	BigInteger sum;
	BigInteger oneBack;
	BigInteger twoBack;
    
	public void init()
	{
	   //add(button1);
		add(label1);
		add(text1);
		add(button1);
		add(text2);
		button1.addActionListener(this);   
	}
	public void paint(Graphics g)
	{
	   setSize(1000,600);		
	   label1.setBounds(20, 20, 300, 20);	   
	   text1.setBounds(330, 20, 150, 20);
	   button1.setBounds(200, 50, 100, 20);
	   text2.setBounds(20,80, 970, 500);
	}
	
	public void actionPerformed(ActionEvent event)
	{
		int i=0;
		String temp;
		 Object source =event.getSource();
		 if (source.equals(button1))
		 {
			 text2.setText("");
			 //text1.setText("bang");
			 temp=text1.getText();
			 i=Integer.parseInt(temp);
			 if(i<4001 && i>0)
			 {
			   fib(i);
			 }
			 else
			 {
			   JOptionPane.showMessageDialog(null, "Enter a value between 1 and 4000");
			   text1.setText("");			   
			 }				 
		 }
	}
	public  void fib(int n)  
	{                             
		sum=new BigInteger("0");
		oneBack=new BigInteger("0");
		twoBack=new BigInteger("0");
		for (int i=0; i<n; i++)
		{
			twoBack=oneBack;			
			if(i<2)
			{
				sum=new BigInteger("1");  
				oneBack=new BigInteger("1");
				twoBack=new BigInteger("0");					
			}
			else
			{
			  oneBack=sum;
			  sum=oneBack.add(twoBack);
			}
			text2.append(Integer.toString(i+1)+").  "+sum.toString()+"\n");			
		}
	}   
}